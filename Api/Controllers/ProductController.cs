using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.DataAccess.Interfaces;
using Api.Repositories.Interfaces;
using Api.Responses;
using Entities;
using Entities.Dto;
using Entities.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository _productrepository;

        public ProductController(IProductRepository productrepository)
        {
            _productrepository = productrepository;
        }

        [HttpGet]
        [Route("api/[controller]")]

        public async Task<IActionResult> GetProducts()
        {
            var response = new Response<IEnumerable<Products>>();
            var products = await _productrepository.GetAllProducts();

            response.Data = products;

            return Ok(response);
        }

        [HttpGet]
        [Route("api/[controller]/Search/{ID}")]

        public async Task<IActionResult> SearchByID(string ID)
        {
           var response = new Response<IEnumerable<Products>>();
           var products = await _productrepository.SearchByID(ID);

           response.Data = products;

           return Ok(response);

        }   


        [HttpGet]
        [Route("api/[controller]/{id}")]

        public async Task<IActionResult> GetProductById(string id)
        {
            var response = new Response<Products>();
            var products = await _productrepository.GetOneProduct(id);

            if (products == null)
            {
                response.Message = "Product not found";
                response.Errors.Add("The Product was not found");

                return NotFound(response);
            }

            response.Data = products;


            return Ok(response);
        }

        [HttpPost]
        [Route("api/[controller]")]

        public async Task<IActionResult> PostProduct([FromBody] Products products)
        {
            if (products == null)
                return BadRequest();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            var created = await _productrepository.InsertProduct(products);
            
            return Created("api/products/" + products.ID, products);

        }

        [HttpPut]
        [Route("api/[controller]")]
        public async Task<IActionResult> PutProduct([FromBody] Products products)
        {
            if (products == null)
                return BadRequest();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            await _productrepository.UpdateProduct(products);
            
            return NoContent();
        }

        [HttpDelete]
        [Route("api/[controller]/{id}")]
        public async Task<ActionResult<Response<bool>>> DeleteProduct(string id)
        {
            var response = new Response<bool>();
        
            response.Data = await _productrepository.DeleteProduct(id);

            return Ok(response);
            
        }

    }
}