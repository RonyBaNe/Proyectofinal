using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Responses;
using Entities;


namespace Api.Controllers;

[ApiController]
public class PingController : ControllerBase
{

    public PingController()
    {
    }

    [HttpGet]
    [Route("api/[controller]")]
    public ActionResult<string> Ping()
    {
        return Ok("Pong");
    }

}
