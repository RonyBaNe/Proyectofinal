using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Api.DataAccess.Interfaces;
using Api.Repositories.Interfaces;
using Dapper;
using Entities;
using Entities.Dto;
using Entities.Http;
using MySql.Data.MySqlClient;

namespace Api.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly IData _data;

        public ProductRepository(IData data)
        {
            _data = data;
        }

        public async Task<IEnumerable<Products>> GetAllProducts()
        {

            var db = _data.DbConnection;


            var sql = "Select * From products";

            return await db.QueryAsync<Products>(sql, new {});
        }

        public async Task<Products> GetOneProduct(string ID)
        {
            var db = _data.DbConnection;


            var sql = @"SELECT * 
                        FROM products WHERE 
                        ID=LPAD(@ID, 8, '0')";

            return await db.QueryFirstOrDefaultAsync<Products>(sql, new {ID = ID});
        }

        public async Task<bool> InsertProduct(Products products)
        {
            
            using (MySqlConnection cn = new MySqlConnection("Server=localhost;Port=3306;Database=proyectofinal;Uid=root;Pwd=admin;AllowUserVariables=True;"))
            {
                var cmd = new MySqlCommand("SELECT COUNT(*) FROM products WHERE ID = LPAD(@ID, 8, '0')", cn);

                cmd.Parameters.AddWithValue("@ID", products.ID);

                cn.Open();

                if (Convert.ToInt32(cmd.ExecuteScalar()) == 0)
                {
              
                    var db = _data.DbConnection;


                    var sql = @"INSERT INTO products (ID, Description) 
                                Values (LPAD(@ID, 8, '0'), @Description)";
                    

                    var result =  await db.ExecuteAsync(sql, new {products.ID, products.Description});

                    return result >  0;
                }
                else
                {
                    
                    return false;
                }
            }
          
        }

        public async Task<bool> UpdateProduct(Products products)
        {
            
           var db = _data.DbConnection;


            var sql = @"UPDATE products SET Description = @Description
                        WHERE ID =LPAD(@ID, 8, '0')";

            var result =  await db.ExecuteAsync(sql, new {products.Description, products.ID});

            return result >  0;
        }

        public async Task<bool> DeleteProduct(string ID)
        {
            var db = _data.DbConnection;


            var sql = @"DELETE 
                        FROM products 
                        WHERE ID=LPAD(@ID, 8, '0')";

            var result = await db.ExecuteAsync(sql, new {ID = ID});

            return result > 0;
        }

         public async Task<IEnumerable<Products>> SearchByID(string ID)
        {
            var db = _data.DbConnection;

            var sql2 = "SELECT * FROM products where ID LIKE '%"+ID+"%' OR Description LIKE  '%"+ID+"%'";
            
            
            return await db.QueryAsync<Products>(sql2, new {});
        }

    }
}